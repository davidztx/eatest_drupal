<?php

namespace Drupal\ea_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ea_test\EATestCarList;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for EnergyAustralia Coding Test page
 */
class EATestController extends ControllerBase
{

  /**
   * @var \Drupal\ea_test\EATestCarList
   */
  protected $carList;

  /**
   * EATestController constructor.
   *
   * @param \Drupal\ea_test\EATestCarList $carList
   */
  public function __construct(EATestCarList $carList) {
    $this->carList = $carList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ea_test.car_list')
    );
  }

  /**
   * Coding Test.
   *
   * @return array
   */
  public function eaTest() {

    $carList = $this->carList->getList();

    if(isset($carList['exception'])) {
      \Drupal::logger('ea_test')->error($carList['exception']->getMessage(), array('exception' => $carList['exception']));
      return [
        '#markup' => $this->t('Service temporarily unavailable.'),
        ];
    } else {
      return $carList;
    }
  }
}