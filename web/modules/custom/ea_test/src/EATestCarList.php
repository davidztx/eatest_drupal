<?php

namespace Drupal\ea_test;

use GuzzleHttp\Client as GuzzleClient;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Create a list of cars
 */
class EATestCarList
{

  use StringTranslationTrait;

  /**
   * Returns the car list
   */
  public function getList()
  {

    $client = new GuzzleClient([
      'base_uri' => 'http://eacodingtest.digital.energyaustralia.com.au/api/v1/',
    ]);

    try {
      $response = $client->get('cars', []);
    } catch (\Exception $e) {
      return [
        'exception' => $e,
      ];
    }
    $carShowsData = json_decode($response->getBody(), TRUE);
    if (empty($carShowsData)) {
      return [
        '#markup' => $this->t('Car list is empty.')
      ];
    }
    /* build a list of the models of cars
      $carlist =>
        'make' =>
          'make_name' (string)
          'models' =>
            'model_name'(string)
            'shows' =>
              'show_name'(string)
    */
    $carList = [];
    foreach ($carShowsData as $carShowKey => $carShow) {
      // Check if the show name has been set.
      // If not set a missing show name
      if (isset($carShow['name']) && !empty($carShow['name'])) {
        $carShowName = $carShow['name'];
      } else {
        $carShowName = 'missing_show_' . $carShowKey;
      }
      foreach ($carShow['cars'] as $car) {
        $make = $car['make'];
        $makeKey = str_replace(' ', '_', strtolower(trim($make)));
        // Check if the model name has been set.
        // If not set a missing model name
        if (isset($car['model']) && !empty($car['model'])) {
          $model = $car['model'];
          $modelKey = 'model_' . str_replace(' ', '_', strtolower(trim($model)));
        } else {
          $model = 'missing_model_' . time();
          $modelKey = 'model_' . time();
        }
        $carList[$makeKey]['name'] = $make;
        $carList[$makeKey]['models'][$modelKey]['name'] = $model;
        $carList[$makeKey]['models'][$modelKey]['shows']['show_' . $carShowKey] = $carShowName;
      }
    }

    ksort($carList);
    return [
      '#theme' => 'ea_carlist',
      '#items' => $carList,
    ];

  }
}
